# UML Analysis

The aim of the project is to develop tools for *static* analysis of UML models. The project started in a student initiative program at Universidad Politécnica de Madrid during summer 2020.

## Mensaje inicial (Spanish)

La verdad es que tengo una idea general sobre lo que quiero pero nada concreto.

Intento centrar un poco el tiro con vosotros.

Os voy dejando ideas, referencias e ideas laterales.

La idea es analizar modelos de UML (no vamos a pasar de diagramas de clases, objetos, estado y secuencias).

Ref: UML Distilled. Martin Fowler.

Seguro: quiero que el lenguaje para describir los modelos sea textual.

### Detalles

Originalmente pretendía analizar sobre el lenguaje de PlantUML pero empiezo a pensar en iniciar una herramienta nueva con una gramática concreta nueva.

Ref: https://www.reddit.com/r/emacs/comments/hruyqu/seeking_advice_on_textual_description_based/

¿Por qué no la misma gramática PlantUML? Porque su gramática está más orientada a *layout* que a semántica. Yo prefiero que nuestra herramienta de análisis hable más de multiplicidades, por ejemplo, que de arriba-abajo-derecha-izquierda.

Idea lateral: si quitamos la información de *layout* de nuestra gramática, entonces, cuando queramos un diagrama, la herramienta tendrá que saber dibujarlo bonito.

Para este proyecto yo me pondría como objetivo: comprobar la consistencia de diagramas de clases y de objetos.

Para tener ejemplos, se me ocurre que podemos modelizar (simplificadamente) las redes sociales que nos apetezca: twitter, insta, discord, slack, ... o cualquier otro sistema popular que queramos. Esos pueden ser nuestros *benchmarks*.

Si creamos un nuevo lenguaje, me gustaría que su base sea muy formal. Por ejemplo, un diagrama como este (PlantUML):

```
class A
class B
A "1" - "1..*" B: a
```

Significa que la asociación `a` es una función total de `B` en `A`. Podemos usar sintaxis como esta:

```
a: B -> A
```

Nos tendremos que inventar sintaxis para las alternativas relevantes. Por ejemplo, para

```
class A
class B
A "*" - "*" B: a
```

Podemos decir que a es una relación (subconjunto del producto cartesiano *A x B*):

```
a: A x B
```

### Sintaxis y Semántica

Estoy terminando. No os asustéis con este párrafo. ¿UML? ¿Tiene que ser UML? La respuesta es NO. Podemos inventarnos lo que queramos para modelizar. Hay otros lenguajes de modelado: Entity-Relationship (ER), podemos usar referencias al mundo DDD (Domain Driven Design) y hablar de entities, value-objects, agregates, contexts, etc. Es más, algo que no me mola nada de UML o de ER o incluso de DDD es que se centran en las *clases* y no en las *asociaciones*. La semántica de los datos no está en las entidades, está en las asociaciones, si creamos un lenguaje hay que centrarse en las relaciones y para ello podemos usar inspiración en *lenguajes relacionales*.

- Ref: Domain-Driven Design: Tackling Complexity in the Heart of Software. Eric Evans.
- Ref: Relational Language and Relational Thought. Dedre Gentner and Jeffrey Loewenstein.
- Ref: First-order unification using variable-free relational algebra. E. J. Gallego, J. Lipton, P. Nogueira and J. Mariño

### Lenguaje de programación y "compilador"

Ya me he extendido bastante en este mensaje. Termino con lo más importante: ¿Qué lenguaje usamos para implementar? ¿Qué tipo de herramienta queremos?

Mi propuesta de lenguaje es Haskell, el tipo de herramienta es un ejecutable como un compilador.

Como referencia para hacer compiladores, yo siempre tengo los libros de Andrew Appel:

Ref: https://www.cs.princeton.edu/~appel/modern/ml/

### Otros temas

Temas no tan importantes. Si os parece nos organizamos en Telegram con un grupo. Alguien e anima a crearlo? Un poquito más adelante abrimos un repo Git en GitLab o GitHub.
